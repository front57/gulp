const {log} = console;

function stepForm(){
    const nextBtn = document.querySelector('.form__btn--next');
    const prevBtn = document.querySelector('.form__btn--prev');
    const steps = document.querySelectorAll('.form__step');
    const stepNumbers = document.querySelectorAll('.form__step-number');
    const progressBar = document.querySelector('.progress__bar');
    const progressVal = document.querySelector('.progress__value');

    let currentStep = 0;

    nextBtn.addEventListener('click',  () => {
        if (currentStep !== steps.length) {
            //stepNumbers[currentStep].classList.remove('active-number')
            currentStep++;
        }
        updateStep();
    });

    prevBtn.addEventListener('click', () =>{
        currentStep--;
        stepNumbers[currentStep + 1].classList.remove('active-number');
        updateStep();
    });

    function updateStep(){
        steps.forEach(step => {
            step.classList.contains('active') && step.classList.remove('active');
        });

        stepNumbers[currentStep].classList.add('active-number')
        steps[currentStep].classList.add('active')


        if (currentStep === 0){
            prevBtn.disabled = true;
        }else{
            prevBtn.disabled = false
        }

        if(currentStep === steps.length - 1){
            nextBtn.disabled = true;
        }else{
            nextBtn.disabled = false;
        }
        const activeSteps = document.querySelectorAll('.active-number');
        const percentage =  ((activeSteps.length - 1) / (steps.length - 1)) * 100 + '%';
        progressVal.innerText = percentage;
        progressBar.style.background = `linear-gradient(to right, #000 ${percentage}, #fff ${percentage})`;
    }

    updateStep()
}

stepForm();