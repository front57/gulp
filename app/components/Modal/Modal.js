function callModals(){
    const modalBtn = document.querySelectorAll('.modal__btn');
    const modalClsBtn = document.querySelectorAll('.modal__close');
    const modals = document.querySelectorAll('.modal');

    modalBtn.forEach(e => e.addEventListener('click', function(){
        this.parentElement.classList.add('active');
    }));

    modalClsBtn.forEach(e => e.addEventListener('click', function(){
        this.closest('.modal').classList.remove('active');
    }));

    window.addEventListener('click', function(e){
        if(e.target.classList.contains('modal__window')){
            modals.forEach(e => e.classList.remove('active'));
        }
    });
}
//callModals();